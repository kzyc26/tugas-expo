import React,{ Component } from 'react';
import {
    Platform,
    StyleSheet,
    Text,
    View,
    Image,
    TouchableOpacity
} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import Icon2 from 'react-native-vector-icons/MaterialIcons';
export default class SkillItem extends Component{
    render(){
        let skill = this.props.skill;
        return(
          <View style={styles.skillcontainer}>
            <Icon style={styles.logo} name={skill.iconName} size={100}/>
            <View style={styles.skillDetail}>
             <Text style={styles.skillTitle}>{skill.skillName} </Text>
             <Text style={styles.skillcategory}>{skill.categoryName} </Text>
             <Text style={styles.skillpercentage}>{skill.percentageProgress}</Text>
            </View>
            <Icon2 style={styles.nextButton} name='navigate-next' size={100}/>
          </View>  
          

        )
    }
}
const styles = StyleSheet.create({
skillcontainer:{
flex:1,
backgroundColor:'#B4E9FF',
flexDirection:'row',
maxHeight:120,
width:370,
marginLeft:10,
marginTop:10,
borderRadius:10
},
skillDetail:{
    flexDirection:'column',
    marginLeft:10,
    width:180
},
logo:{
  color:'#003366',
},
skillTitle:{
color:'#003366',
fontSize:20,
fontWeight:'bold',
fontFamily: 'Roboto',
fontStyle: 'normal',
},
skillcategory:{
    color:'#3EC6FF',
    fontSize:12,
    fontWeight:'bold',
    fontFamily: 'Roboto',
    fontStyle: 'normal',
},
skillpercentage:{
    left:70,
    color:'white',
    fontSize:30,
    fontWeight:'bold',
    fontFamily: 'Roboto',
    fontStyle: 'normal',
},
nextButton:{
    color:'#003366',
    top:5
}
})