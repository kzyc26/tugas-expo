import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  Image,
  TouchableOpacity,
  FlatList
} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';
export default class App extends Component{
    render(){
        return(
            <View style={styles.container}>
                <Text style={styles.Title}>Tentang Saya</Text>
                <Icon style={styles.profPicture} name="account-circle" size={150} />
                <Text style={styles.profname}>Mukhlis Hanafi</Text>
                <Text style={styles.profrole}>React Native Developer</Text>
                <View style={styles.box}>
                    <View style={styles.aboutTitleContainer}>
                    <Text style={styles.aboutTitle}>Portofolio</Text>
                    </View>
                    <View style={styles.portfolioContainer }>
                        <View style={styles.portfolioContent}> 
                        <Image source={require('./images/gitlab.png')}  style={styles.logo}/>
                        <Text style={styles.contentText}>@mukhlish</Text>
                        </View>
                        <View style={styles.portfolioContent}>
                        <Image source={require('./images/github.png')}  style={styles.logo}/>
                        <Text style={styles.contentText}>@mukhlis-h</Text>
                        </View>
                    </View>
                </View>
                <View style={styles.contactbox}>
                <View style={styles.aboutTitleContainer}>
                        <Text style={styles.aboutTitle}> Hubungi Saya</Text>
                        </View>
                    <View style={styles.Contactcontainer}>
                        <View style={styles.contactdetails}>
                        <Image source={require('./images/facebook.png')}  style={styles.logo}/>
                        <Text style={styles.contentText}>mukhlis.hanafi</Text>
                        </View>
                        <View style={styles.contactdetails}>
                        <Image source={require('./images/instagram.png')}  style={styles.logo}/>
                        <Text style={styles.contentText}>@mukhlis_hanafi</Text>
                        </View>
                        <View style={styles.contactdetails}>
                        <Image source={require('./images/twitter.png')}  style={styles.logo}/>
                        <Text style={styles.contentText}>@mukhlish</Text>
                        </View>
                    </View>
                </View>
              
            </View>
            

        )
    }
}
const styles = StyleSheet.create({
    container: {
        flex: 1
      },
    Title:{
    position: 'relative',
    top: 80,
    textAlign:'center',
    fontFamily:'Roboto',
    fontStyle:'normal',
    fontWeight:'bold',
    fontSize: 20,
    lineHeight: 28,
    color: '#003366',
},
profPicture:{
    color:'#888888',
    textAlign:'center',
    top:80
},
profname:{
    position: 'relative',
    textAlign:'center',
    top: 80,
    fontFamily: 'Roboto',
    fontStyle: 'normal',
    fontWeight: 'bold',
    fontSize: 20,  
    color: '#003366',
},
profrole:{
    position: 'relative',
    textAlign:'center',
    top: 80,
    fontFamily: 'Roboto',
    fontStyle: 'normal',
    fontWeight: 'bold',
    fontSize: 12,  
    color: '#3EC6FF',
},

box:{
    flex:3,
    position:"relative",
    width: 350,
    top:100,
    maxHeight:140,
    marginLeft:20,
    backgroundColor: '#EFEFEF',
    borderRadius: 16,
    marginBottom:10
},
contactbox:{
    flex:3,
    position:"relative",
    width: 350,
    top:100,
    maxHeight:200,
    marginLeft:20,
    backgroundColor: '#EFEFEF',
    borderRadius: 16,
    marginBottom:10
},

aboutTitleContainer:{
position:'relative',
top:0,
marginLeft:5,
borderBottomWidth:1,
borderBottomColor:'#003366',
marginTop:10
},

aboutTitle:{
color:'#003366',
},
portfolioContainer:{
    flexDirection: 'row',
    position:'relative',
    top:0,
},
portfolioContent:{
alignItems:'center',
marginLeft:40,
marginRight:40,
marginTop:10,

},
logo:{
    width: 40,
    height: 40,
    marginRight:10,
    resizeMode: 'contain'
},
contentText:{
    position: 'relative',
    textAlign:'center',
    fontFamily: 'Roboto',
    fontStyle: 'normal',
    fontWeight: 'bold',
    fontSize: 12,  
    marginTop:5,
    color: '#003366',  
},
contactdetails:{
    textAlign:'center',
    position:'relative',
    flexDirection:'row',
    marginTop:10,

},
Contactcontainer:{
    top:0,
    marginLeft:80
}
})