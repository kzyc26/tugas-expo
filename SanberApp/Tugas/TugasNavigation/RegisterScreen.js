import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  Image,
  TouchableOpacity,
  FlatList,
  TextInput,
  LinearGradient
} from 'react-native';
export default class App extends Component{
    render(){
        return(
        <View style={styles.container}>
        <Image source={require('./assets/logo.png')}style={styles.logo}></Image>
        <Text style={styles.pageTitle}>Register</Text>
        <View style={styles.input}>
            <Text style={styles.inputTitle}>Username</Text>
            <TextInput style={styles.inputData}></TextInput>
            <Text style={styles.inputTitle}>Email</Text>
            <TextInput style={styles.inputData}></TextInput>
            <Text style={styles.inputTitle}>Password</Text>
            <TextInput style={styles.inputData}></TextInput>
            <Text style={styles.inputTitle}>Ulangi Password</Text>
            <TextInput style={styles.inputData}></TextInput>
        </View>
            <View style={styles.buttons}>
                <TouchableOpacity style={styles.loginButton}>
                  <Text style={styles.textButton}>Masuk?</Text>
                </TouchableOpacity>
            </View>
        <Text style={styles.Atau}>atau</Text>
        <View style={styles.buttons}>
                 <TouchableOpacity style={styles.signupButton}>
                 <Text style={styles.textButton}>Daftar</Text>
                </TouchableOpacity>
        </View>
        </View>
        );
    }
}
const styles = StyleSheet.create({
    container: {
      flex: 1
    },
    logo:{
        width: 375,
        height: 102,
        left: 0,
        top: 63,
       },
       pageTitle:{
        position: 'relative',
        top: 100,
        textAlign:'center',
        fontFamily:'Roboto',
        fontStyle:'normal',
        fontWeight:'normal',
        fontSize: 20,
        lineHeight: 28,
        color: '#003366',
       },
    input:{
        top: 130 
    },

       inputTitle:{
        position:'relative',
        left: 40,
        paddingBottom:5,
        fontFamily:'Roboto',
        fontStyle:'normal',
        fontWeight:'normal',
        fontSize: 10,
        lineHeight: 19,
        color: '#003366',
        justifyContent:'space-around'
       },
       inputData: {
        borderWidth: 1,
        paddingBottom:10,
        position: 'relative',
        width: 294,
        left: 40,
        borderColor:'#003366',
        justifyContent:'space-around'
       },
       loginButton:{
        position: 'relative',
        top : 150,
        borderRadius:16,
        backgroundColor:'#3EC6FF',
       },
       signupButton:{
        position: 'relative',
        top : 150,
        borderRadius:16,
        backgroundColor:'#003366',
       },
       Atau:{
        position: 'relative',
           left:165,
           top: 145,
           color:'#3EC6FF',
           fontFamily:'Roboto',
           fontStyle:'normal',
           fontWeight:'normal',
           fontSize: 15,
           lineHeight: 19,
       },
       textButton:{
        textAlign:'center',
        color:'white'
       },
       buttons:{
        width:100,
        marginBottom:10,
        marginTop:10,
        height:30,
        left:140
       }
  });