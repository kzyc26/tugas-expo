import React from "react";
import { StyleSheet, Text, View } from "react-native";
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import { createDrawerNavigator } from "@react-navigation/drawer";

import{default as About} from './AboutScreen';
import{default as Add} from './AddScreen';
import{default as Login} from './LoginScreen';
import{default as Project} from './ProjectScreen';
import{default as Skill} from './SkillScreen';
import{default as Register} from './RegisterScreen';
import { ScreenContainer } from "react-native-screens";

const Tabs = createBottomTabNavigator();
const TabsStackScreen = () => (
  <Tabs.Navigator>
    <Tabs.Screen name="Skill" component={SkillStackScreen} />
    <Tabs.Screen name="Project" component={ProjectStackScreen} />
    <Tabs.Screen name="Add" component={AddStackScreen} />
  </Tabs.Navigator>
)

const SkillStack = createStackNavigator();
const SkillStackScreen = () => (
  <SkillStack.Navigator>
    <SkillStack.Screen name="Skill" component={Skill} />
  </SkillStack.Navigator>
)

const ProjectStack = createStackNavigator();
const ProjectStackScreen = () => (
  <ProjectStack.Navigator>
    <ProjectStack.Screen name="Project" component={Project} />
  </ProjectStack.Navigator>
)

const AddStack = createStackNavigator();
const AddStackScreen = () => (
  <AddStack.Navigator>
    <AddStack.Screen name="Add" component={Add} />
  </AddStack.Navigator>
)

const LoginStack = createStackNavigator();
const LoginStackScreen = () => (
  <LoginStack.Navigator>
    <LoginStack.Screen name="Login" component={Login} />
    <LoginStack.Screen name="Register" component={Register} options={{title: "Register"}} />
  </LoginStack.Navigator>
)

const AboutStack = createStackNavigator();
const AboutStackScreen = () => (
  <AboutStack.Navigator>
    <AboutStack.Screen name="About" component={About} />
  </AboutStack.Navigator>
)
const Drawer = createDrawerNavigator();

export default () => (
  <NavigationContainer>
    <Drawer.Navigator>
      <Drawer.Screen name="Login" component={LoginStackScreen} />
      <Drawer.Screen name="About" component={AboutStackScreen} />
      <Drawer.Screen name="Skill Screen" component={TabsStackScreen} />
    </Drawer.Navigator>
  </NavigationContainer>
)

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    alignItems: "center",
    justifyContent: "center"
  }
});
