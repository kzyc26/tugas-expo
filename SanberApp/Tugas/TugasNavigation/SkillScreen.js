import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  Image,
  TouchableOpacity,
  FlatList,
  ScrollView
} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';
import data from './skillData.json';
import SkillItem from './components/Skills';
export default class App extends Component {
    render(){
        return(
            <View style={styles.container}>
                <Image source={require('./assets/logo.png')} style={styles.mainLogo}/>
                <View style={styles.userProfile}>
                <Icon style={styles.profPicture} name="account-circle" size={40} />
                <View style={styles.userGreetings}>
                <Text style={styles.greetings}>Hai,</Text>
                <Text style={styles.username}>Mukhlis Hanafi</Text>
                </View>
                </View>
                <View style={styles.pagetitle}>
                <Text style={styles.title}>SKILL</Text>
                </View>
                <View style={styles.category}>
                <Text style={styles.categorytext}>Library/Framework</Text>
                <Text style={styles.categorytext}>Bahasa Pemrograman</Text>
                <Text style={styles.categorytext}>Teknologi</Text>
                </View>
                <ScrollView style={styles.Body}>
                <FlatList
                    data={data.items}
                    renderItem={(skill)=><SkillItem skill={skill.item} />}
                    keyExtractor={(item)=>item.id}
                    />
                </ScrollView>
            </View>
        )
    };
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor:'white'
      },
      mainLogo:{
        marginTop:0,
        width: 190,
        height: 70,
        resizeMode: 'contain',
        flexDirection:'row',
        alignSelf:'flex-end'
      },
      profPicture:{
          color:'#3EC6FF',
          marginTop:0

      },
      userProfile:{
          flexDirection:'row',
        marginLeft:10,
      },
      userGreetings:{
          flexDirection:'column',
          marginLeft:10
      },
      greetings:{
        fontSize:10,
      },
      username:{
          color:'#003366',
      },
      pagetitle:{
          marginTop:10,
          borderBottomColor:'#3EC6FF',
          borderBottomWidth:5,
          width:370,
          marginLeft:10
      },
      title:{
        fontFamily: 'Roboto',
        fontStyle: 'normal',
        fontWeight: 'normal',
        fontSize:20,  
        color: '#003366',
      },
      category:{
        flexDirection:'row'
      },
      categorytext:{
        backgroundColor:'#b4e9ff',
        borderRadius:5,
        color : '#003366',
        fontSize:10, 
        maxWidth:250,
        padding:5,
        marginLeft:10,
        marginTop:5,
        textAlign:'center',
      },
      Body:{
      
      }
})